import React from 'react'
import PageHeader from '../template/pageHeader'

export default props => (
    <div>
        <PageHeader name={'Sobre'} small={'Nós'}/>

        <h2>Nossa história</h2>
        <p>Nam fermentum augue felis, ac malesuada tellus pellentesque nec.</p>
        <h2>Missão e Visão</h2>
        <p>Proin cursus lorem in magna volutpat, nec cursus urna sollicitudin.</p>
        <h2>Imprensa</h2>
        <p>Cras condimentum justo vitae nunc posuere euismod.</p>
    </div>
)
