import React from 'react'
import {connect} from 'react-redux'
import TodoItem from './todoItem'

class TodoList extends React.Component {
    render() {
        const list = this.props.todos || []
        return (
            <table className={'table'}>
                <thead>
                <tr>
                    <th>Descrição</th>
                    <th className={'tableActions'}>Ações</th>
                </tr>
                </thead>
                <tbody>{list.map(todo => (
                    <TodoItem key={todo._id} todo={todo}/>))}</tbody>
            </table>
        )
    }
}

const mapStateToProps = (state) => ({todos: state.todo.list})

export default connect(mapStateToProps)(TodoList)
