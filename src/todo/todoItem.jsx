import React from 'react'
import IconButton from '../template/iconButton'
import {markAs, remove} from './todoActions'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

class TodoItem extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <tr>
                <td className={this.props.todo.done ? 'markedAsDone' : ''}>{this.props.todo.description}</td>
                <td>
                    <IconButton style={'success'} icon={'check'}
                                onClick={() => this.props.markAs(this.props.todo, true, this.props.description)}
                                hide={this.props.todo.done}/>
                    <IconButton style={'warning'} icon={'undo'}
                                onClick={() => this.props.markAs(this.props.todo, false, this.props.description)}
                                hide={!this.props.todo.done}/>
                    <IconButton style={'danger'} icon={'trash-o'}
                                onClick={() => this.props.remove(this.props.todo, this.props.description)}
                                hide={!this.props.todo.done}/>
                </td>
            </tr>
        )
    }
}

const mapStateToProps = state => ({description: state.todo.description})
const mapDispatchToProps = dispatch => bindActionCreators({markAs, remove}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TodoItem)
