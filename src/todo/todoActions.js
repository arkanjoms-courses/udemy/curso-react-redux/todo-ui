import axios from 'axios'

const URL = 'http://localhost:3003/api/todos'

export const changeDescription = event => ({
    type: 'DESCRIPTION_CHANGED',
    payload: event.target.value
})

export const search = async (description) => {
    const search = description ? `&description__regex=/${description}/` : ''
    const data = await axios.get(`${URL}?sort=-createAt${search}`)
    return {
        type: 'TODO_SEARCHED',
        payload: data
    }
}

export const add = async description => {
    await axios.post(URL, {description})
    return [clear(), search()]
}

export const markAs = async (todo, done, description) => {
    await axios.put(`${URL}/${todo._id}`, {...todo, done})
    return search(description)
}

export const remove = async (todo, description) => {
    await axios.delete(`${URL}/${todo._id}`)
    return search(description)
}

export const clear = () => {
    return [{type: 'TODO_CLEAR'}, search()]
}
